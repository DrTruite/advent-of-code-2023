"""
This is Victor Chavanne's answers to day 2 of advent of code 2023 puzzles.
"""


def game_is_possible(line: str, possible_game: dict) -> int:
    """Finds the score of the game. 0 if the game is not possible.

    :param line: The line to analyse.
    :param possible_game: The maximum numnber of each cube.
    :return: The score of the game.
    """
    game_id, games = line.split(":")
    for game in games.split(";"):
        for play in game.split(","):
            if possible_game[play.split()[1]] < int(play.split()[0]):
                return 0
    return int(game_id.split()[1])


def puzzle_one() -> int:
    """Find the answer to the first puzzle of day 02.

    :return: The sum of the plausible IDs
    """

    result_sum = 0
    possible_game = {
        "red": 12,
        "green": 13,
        "blue": 14,
    }
    with open("inputs/day_02", "r", encoding="utf_8") as input_file:
        for line in input_file:
            result_sum += game_is_possible(line, possible_game)
    return result_sum


def game_power(line: str) -> int:
    """Calculate the power score of a game

    :param line: The line to analyse.
    :return/ The power score of the game.
    """
    _, games = line.split(":")
    rgb = {"red": 0, "green": 0, "blue": 0}
    for game in games.split(";"):
        for play in game.split(","):
            rgb[play.split()[1]] = max(rgb[play.split()[1]], int(play.split()[0]))
    return rgb["red"] * rgb["green"] * rgb["blue"]


def puzzle_two() -> int:
    """Find the answer to the second puzzle of day 02.

    :return: The sum of the games power
    """

    result_sum = 0

    with open("inputs/day_02", "r", encoding="utf_8") as input_file:
        for line in input_file:
            result_sum += game_power(line)
    return result_sum


if __name__ == "__main__":
    print(f"Answer to first puzzle is : {puzzle_one()}")
    print(f"Answer to second puzzle is : {puzzle_two()}")
