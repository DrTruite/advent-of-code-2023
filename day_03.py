"""
This is Victor Chavanne's answers to day 3 of advent of code 2023 puzzles.
"""

def part_score(engine, pos):
    j, i = pos

    jmin = max(0, j-1)
    jmax = min(j+2, len(engine))
    imin = max(0, i-1)
    imax = min(i+1, len(engine[0]))

    found_imax = False
    while not found_imax:
        if engine[j][imax].isnumeric():
            imax += 1
        else:
            imax += 1
            found_imax = True
    
    is_part = False
    for ji in range(jmin, jmax):
        for ii in range(imin, imax):
            if not engine[ji][ii].isalnum() and engine[ji][ii] != "." and engine[ji][ii] != "\n":
                print(engine[ji][ii])
                is_part = True
                break
    
    if not is_part:
        return 0

    part_num = ""
    for ii in range(i, imax-1):
        part_num += (engine[j][ii])
    
    return int(part_num)


def puzzle_one() -> int:
    """Find the answer to the first puzzle of day 03.

    :return: The sum of all of the part numbers in the engine schematic
    """

    result_sum = 0

    engine = []
    with open("inputs/day_03", "r", encoding="utf_8") as input_file:
        for line in input_file:
            engine.append(list(line))
    
    for j, line in enumerate(engine):
        for i, char in enumerate(line):
            if i == 0 and char.isnumeric():
                result_sum += part_score(engine, (j, i))
            elif i != 0 and not line[i-1].isnumeric() and char.isnumeric():
                result_sum += part_score(engine, (j, i))

    return result_sum


def puzzle_two() -> int:
    """Find the answer to the second puzzle of day 02.

    :return: The sum of the games power
    """

    result_sum = 0

    return result_sum


if __name__ == "__main__":
    print(f"Answer to first puzzle is : {puzzle_one()}")
    print(f"Answer to second puzzle is : {puzzle_two()}")
