# Advent of Code 2023

## Description
My personal go at the [Advent of Code](https://adventofcode.com/) 2023 puzzles.  
⭐⭐⭐⭐

## Installation
Install dependencies with poetry:
```console
poetry install
```
Or install all dependencies (including dev) with poetry:
```console
poetry install --with dev
```

## Usage
Run individual days simply by running the file
```console
python day_01.py
```
or individual puzzles from python:
```python
import day_01
puzzle_one_answer = day_01.puzzle_one()
print(puzzle_one_answer)
```