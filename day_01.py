"""
This is Victor Chavanne's answers to day 1 of advent of code 2023 puzzles.
"""

import re


def puzzle_one():
    """Find the answer to the first puzzle of day 01.

    :return: The sum of hidden numbers
    :rtype: int
    """

    result_sum = 0
    with open("inputs/day_01", "r", encoding="utf_8") as input_file:
        for line in input_file:
            number_list = [char for char in line if char.isnumeric()]
            number = int(number_list[0] + number_list[-1])
            result_sum += number

    return result_sum


def puzzle_two():
    """Find the answer to the first puzzle of day 02.

    :return: The sum of hidden numbers
    :rtype: int
    """

    regex = r"(?=(zero|one|two|three|four|five|six|seven|eight|nine|[0-9]))"
    number_dict = {
        "zero": "0",
        "one": "1",
        "two": "2",
        "three": "3",
        "four": "4",
        "five": "5",
        "six": "6",
        "seven": "7",
        "eight": "8",
        "nine": "9",
    }
    result_sum = 0
    with open("inputs/day_01", "r", encoding="utf_8") as input_file:
        for line in input_file:
            number_list = [
                number_dict[num] if num in number_dict else num
                for num in re.findall(regex, line)
            ]
            number = int(number_list[0] + number_list[-1])
            result_sum += number

    return result_sum


if __name__ == "__main__":
    print(f"Answer to first puzzle is : {puzzle_one()}")
    print(f"Answer to second puzzle is : {puzzle_two()}")
